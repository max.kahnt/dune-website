+++
major_version = 2
minor_version = 2
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 1
title = "Dune 2.2.1"
version = "2.2.1"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.2.1 - Release Notes

## Build system

*   Fixed test for support of `__attribute__((unused))`
*   Support for automake 1.13 ([FS 1243](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1243))
*   Made dunecontrol faster
*   dunecontrol can handle multiple installed dune-common modules ([FS 1250](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1250))
*   dunecontrol has better support for suggestions
*   Build system finds ParMETIS 4.0.2 ([FS 1163](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1163))
*   Out-of-source builds of the documentation are supported now.

## dune-common

*   Fixed warnings about unused typedefs.
*   More standard compliant shared_ptr implementation usable with gcc-4.1.
*   More standard compliant nullptr implementation
*   Fixed illegal forward declaration of `std::pair` ([FS 1121](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1121))
*   Fixed infinity norm of NaN matrices ([FS 1147](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1147))
*   The `infinity_norm` of a `DenseVector` returns NaN if one of its entries is NaN ([FS 1147](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1147))
*   Improved support for clang compiler

## dune-geometry

*   Improved support for clang compiler
*   Fixed compilation for zero-dimensional grids

## dune-grid

*   Better support for clang
*   Detect and support alberta grid configured with --disable-graphics
*   Fixed compilation of gmsh2dgf [FS 929](http://www.dune-project.org/flyspray/index.php?do=details&task_id=929))
*   UGGrid switched to using `static_cast` from C-style casts.
*   Fixed support of UG with gcc 4.6.3 on OpenSuse 11.3 ([FS 1142](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1142))
*   Fixed ALUGridFactory on MacOS.
*   Allow compilation of subsequent modules with -D<gridtype> if dune-grid is not used ([FS 1203](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1203))

## dune-istl

*   Reduced warnings when compiling with clang.
*   We now use `template` to qualify member templates of class templates ([FS 1251](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1251)).
*   We now always initialize data memory in `BCRSMatrix` ([FS 1041](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1041))
*   Improved support for `std::complex` in Krylov solvers.
*   Fixed `dbgs` implementation ([FS 1170](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1170))
*   Fixed wrong assertion in `matMultTransposeMult` ([FS 1171](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1171))
*   `FieldMatrix` can now be constructed from a `DiagonalMatrix`
*   `AMG` now detects Dirichlet boundary conditions and updates the left hand side x during `pre` such that A_dd*x_d=b_d holds.
*   Allow setting `setSkipIsolated(true)` in `AMG`'s coarsening criterion.
*   Fixed call of METIS function.

## dune-localfunctions

No mentionable changes.

## dune-grid-howto

No mentionable changes.

## dune-grid-dev-howto

No mentionable changes.
