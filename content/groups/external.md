+++
group = "external"
title = "External Modules"
[menu.main]
parent = "modules"
weight = 4
+++

### External Modules

DUNE has a modular structure and it is possible to write your own DUNE modules. This is a list of a few externally maintained extension modules that we know about. There are many more DUNE modules around maintained by different groups.

### Highlevel Applications & Simulation Frameworks

These are actually not Dune modules, but general software packages that use Dune in one way or another for infrastructure.

* [DuMu<sup>x</sup>](http://www.dumux.org/) is a DUNE based simulator for flow and transport processes in porous media.
* [Kaskade 7](http://www.zib.de/en/numerik/software/kaskade-7.html) uses Dune for the grid and linear algebra infrastructure.
* The [Open Porous Media Simulator (OPM)](http://opm-project.org/) initiative has the goal to develop a DUNE based simulation suite that is capable of modeling industrially and scientifically relevant flow and transport processes in porous media.
* [BEM++](http://www.bempp.org/) is an open source C++/Python library for boundary element methods.

### Others

* [dune-solvers](http://numerik.mi.fu-berlin.de/dune/dune-solvers) contains a variety of algebraic solvers structured in a hierarchy with dynamical polymorphism.
