+++
title = "Binary Packages"
[menu.main]
parent = "releases"
weight = 2
+++

### Debian

Debian packages for Dune releases are available starting with the 2.2.0 release.
Please consider using these packages before building them yourself.

The packages are called `libdune-foo-dev`, where `foo` is a module name like for example
`common`, `grid`, `geometry`, `istl`, or `localfunctions`.  A  [complete list of packages](http://packages.debian.org/search?keywords=libdune&searchon=names&suite=all&section=all)
is available.

If you want to help with the Debian packaging, you can download the packaging information for `dune-common` by

`git clone git://anonscm.debian.org/debian-science/packages/dune-common.git`
([Browse Repository](https://anonscm.debian.org/git/debian-science/packages/dune-common.git))

and similarly for all other available modules.

### Ubuntu

The packages are called `libdune-foo-dev`, where `foo` is a module name like for example
`common`, `grid`, `geometry`, `istl`, or `localfunctions`.  A  [complete list of packages](http://packages.ubuntu.com/search?keywords=libdune&searchon=names&suite=all&section=all)
is available.

For the Ubuntu LTS versions Trusty Tahr (14.04) and Precise Pangolin (12.04) the developers of [OPM](http://www.opm-project.org/)
(Open Porous Media Initiative) have kindly provided pre-built packages of the 2.3.1 DUNE and ALUGrid release in their PPA.
The DUNE packages are backports of the packages in Ubuntu 14.10 (Utopic Unicorn), i.e. compiled with MPI support.

### openSUSE

RPM packages for openSUSE are called `libdune-foo-devel`, with `foo` as above or `typetree` or `pdelab`.
The packages are available from the [science repository](http://download.opensuse.org/repositories/science/).
