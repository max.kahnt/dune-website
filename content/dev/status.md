+++
title = "Build Status"
[menu.main]
parent = "dev"
weight = 6
+++

## Core Modules

| module      | master | 2.5 |
|-------------|--------|-----|
| dune-common | [![build status](https://gitlab.dune-project.org/core/dune-common/badges/master/build.svg)](https://gitlab.dune-project.org/core/dune-common/commits/master) | [![build status](https://gitlab.dune-project.org/core/dune-common/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/core/dune-common/commits/releases/2.5)
| dune-geometry | [![build status](https://gitlab.dune-project.org/core/dune-geometry/badges/master/build.svg)](https://gitlab.dune-project.org/core/dune-geometry/commits/master) | [![build status](https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.5)
| dune-grid | [![build status](https://gitlab.dune-project.org/core/dune-grid/badges/master/build.svg)](https://gitlab.dune-project.org/core/dune-grid/commits/master) | [![build status](https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.5)
| dune-istl | [![build status](https://gitlab.dune-project.org/core/dune-istl/badges/master/build.svg)](https://gitlab.dune-project.org/core/dune-istl/commits/master) | [![build status](https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.5)
| dune-localfunctions | [![build status](https://gitlab.dune-project.org/core/dune-localfunctions/badges/master/build.svg)](https://gitlab.dune-project.org/core/dune-localfunctions/commits/master) | [![build status](https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.5)

## Staging Modules

| module      | master | 2.5 |
|-------------|--------|-----|
| dune-functions | [![build status](https://gitlab.dune-project.org/staging/dune-functions/badges/master/build.svg)](https://gitlab.dune-project.org/staging/dune-functions/commits/master) | [![build status](https://gitlab.dune-project.org/staging/dune-functions/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/staging/dune-functions/commits/releases/2.5)
| dune-typetree | [![build status](https://gitlab.dune-project.org/pdelab/dune-typetree/badges/master/build.svg)](https://gitlab.dune-project.org/pdelab/dune-typetree/commits/master) | [![build status](https://gitlab.dune-project.org/pdelab/dune-typetree/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/pdelab/dune-typetree/commits/releases/2.5)
| dune-uggrid | [![build status](https://gitlab.dune-project.org/staging/dune-uggrid/badges/master/build.svg)](https://gitlab.dune-project.org/staging/dune-uggrid/commits/master) | [![build status](https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.5)


## Extension Modules

| module      | master | 2.5 |
|-------------|--------|-----|
| dune-grid-glue | [![build status](https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/master/build.svg)](https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/master) | [![build status](https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/releases/2.5/build.svg)](https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/releases/2.5)
