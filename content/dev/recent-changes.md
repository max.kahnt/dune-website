+++
title = "Recent Changes"
[menu.main]
parent = "dev"
weight = 4
+++

## Recent changes in Git master (will become DUNE 2.5)

### Dependencies

In order to build this version of DUNE you need at least the following software:

* CMake 2.8.12 or newer
* pkg-config
* A standard complient C++ compiler supporting C++11 and the C++14 feature set of GCC 4.9.
  We support GCC 4.9 or newer and Clang 3.5 or newer. We try to stay compatible to ICC 15.1 and newer
  but this is not tested.

### Buildsystem

* Adding tests is now done using `dune_add_test` which can take care for adding includes and libraries, and setting properties.
* If accepted by the compiler, we use the C++14 mode by passing the flag `-std=c++14`.
  In any case, we require that the compiler is in C++11 mode or we pass `-std=c++11`.
* The buildsystem got a documentation. It is generated using Sphinx.
* A new check for SuiteSparse was added which replaces the now deprecated check for UMFPack.

#### Deprecated and Removed Features

* The Autotools buildsystem was removed.
* Support for Boost was removed. You can copy the removed CMake files from
  Dune 2.4 and keep them within your module.
* The testing magic was removed. To build the tests use the top-level target
  `build_test`. The target test which redirects to the command `ctest` is only used for executing the tests.

### dune-common

* `Dune::Exception` is now derived from `std::exception`.
* Added macros `DUNE_NO_DEPRECATED_BEGIN` and `DUNE_NO_DEPRECATED_END` which mark a block in which
  deprecation warnings are ignored. This can be useful for implementations of deprecated methods
  that call other deprecated methods or for testing deprecated methods in the test suite.
* `Dune::Std::apply` provides the functionality of `std::apply` by either using the standard library
  or calling Dune's own implementation.
* The new header `hybridutilities.hh` contains various algorithmic primitives
  (`ifElse()`, `equals()`, `size()`, `elementAt`, `forEach()`, ...) that can work
  on classic dynamic values and containers, as well as on static values encoded
  as `integral_constant` and statically sized heterogenous container. This does for example
  include loops over `std::tuple` or an emulation of a `static_if` statement.
* The `size()` method of `FieldVector` is now `static constexpr` such that
  it can be used in static contexts.

#### Deprecated and Removed Features

* Support for MPI older then version 2.1 has been removed, it was marked as
  deprecated in Dune 2.4.
* The macros `DUNE_CONSTEXPR`, `DUNE_FINAL` and `DUNE_NOEXCEPT` are deprecated and will be removed
  in the near future. Use C++'s `constexpr`, `final` and `noexcept` instead.
* Removed own implementation of C++-14' integer sequence. Removed `Dune::Std::declval()`. In both
  cases use C++'s capabilities instead.

### dune-geometry

#### Deprecated and Removed Features

* Removed `ReferenceElement::checkInside<codim>` because nobody used it.

### dune-grid

* The `VTKSequenceWriter` class has a new constructor that takes a `VTKWriter` object from the outside,
  rather than creating one internally. This has several advantages. In particular, it makes the
  `SubsamplingVTKSequenceWriter` class obsolete, because you can now use a `VTKSequenceWriter`
  with a `SubsamplingVTKWriter` instead.
* We have forked upstream UG, and turned it into a genuine Dune module called [dune-uggrid](https://gitlab.dune-project.org/staging/dune-uggrid).
  The new code has already seen considerable cleanup, and nontrivial bugfixes.  From now on, please install and use
  dune-uggrid instead of UG.  The programmer interface of the UGGrid class remains unchanged.

#### Deprecated and Removed Features

* SGrid was removed, use YaspGrid instead.
* ALUGrid was removed, use [dune-ALUGrid](https://gitlab.dune-project.org/extensions/dune-alugrid) instead.
* Support for Grape was removed, use [dune-grape](https://gitlab.dune-project.org/robert.kloefkorn/dune-grape) instead.
* The values `Geometry::dimension` and `Geometry::dimensionworld`, deprecated in Dune 2.4, have been removed. Most likely they should be replaced by `Geometry::mydimension` and `Geometry::coorddimension`.
* `Intersection::codimension` and `Intersection::dimension` are deprecated. The codimension
  is always 1, the dimension can be obtained from the grid or an element.
* The `GridView`'s methods `overlapSize()`, `ghostSize()` and `communicate()` are deprecated and will be
  removed. Use the same methods from `Grid` instead.
* The methods `VTKWriter::addCellData` and `VTKWriter::addVertexData` taking *raw* pointers to `VTKFunction` objects have been removed. Those methods had already been deprecated in dune-grid-2.4. Please hand over VTKFunction objects in `std::shared_ptrs` instead.
* The two `map` methods of the `MultipleCodimMultipleGeomType` class have been removed. They were
  deprecated in 2.4 and have been replaced by new methods `index` and `subIndex` with the same signature.
  With these new names, the `MultipleCodimMultipleGeomType` interface is closer to the `IndexSet` interface.
* `vtkWriter` no longer exports the type `VTKFunctionPtr`; use `std::shared_ptr<const VTKFunction>` instead.

### dune-istl

* The classes `MultiTypeBlockVector` and `MultiTypeBlockMatrix` have received a major overhaul. In particular,
  they don't rely on `boost::fusion` anymore. Also, you can now use `operator[]` to access the individual entries.
* The bindings to Pardiso have been updated. We switch from Fortran bindings to C ones.
* Support for external linear solvers SuiteSparse's LDL (Cholesky / LDL decomposition) and SPQR (Sparse QR factorization).
* SuperLU 5 is supported.

#### Deprecated and removed features

* SuperLU versions prior to 4.0 are no longer supported.

### dune-localfunctions

* The `LocalBasis` classes have gained a new method `partial`, which is used to compute partial derivatives of the shape
  functions.  Previously, the method `LocalBasis::evaluate` existed for this purpose, but it had several shortcomings.
  The new method differs from the old one in that it uses text-book multi-index notation to specify which partial
  derivative to compute.  This seemingly aesthetic change allows to fix several difficult technical problems.  In particular,
  unlike `evaluate`, the `partial` method can be used through the virtual interface.


#### Deprecated and removed features

* The class `MonomLocalFiniteElement`, deprecated in Dune 2.4, has been removed together with its header `monom.hh`.
* The method `LocalBasis::evaluate`, previously used to compute shape functions derivatives of any order, has been deprecated.
  Use the new method `LocalBasis::partial` instead.

### dune-grid-howto

## Known Bugs

* The pseudo inverse used in the generic geometries might fail for nearly singular matrices. This is not really a problem unless the grid is nearly degenerate.
* The parallel UGGrid may return wrong ids on very complex adaptively refined and load-balanced grids. The reason is that the Dune grid interface mandates that two entities on different levels have the same id if they are copies. Therefore the UGGrid id/subId methods look through the ancestry of a given entity to see if there are copies. However, UG does so-called vertical load-balancing, which means that the ancestry may be distributed across different processors. Since the current code does not take that into account, wrong ids will be returned in the presence of vertical load-balancing. This is a potentially severe issue, because users do not get error messages, only ids that are tacitly wrong.
* Building shared libraries with CMake might break if you are using external static libraries compiled without support for position independent code (g++ -fpic).
* Dune-istl does not work with METIS 5, see .
* It is possible that compilation of the tests in dune-common with gcc-4.9 and the options
  '-O3 -g' does not terminate due to a [gcc-4.9 bug](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=77844).
  More information can be found in the corresponding [issue](https://gitlab.dune-project.org/flyspray/FS/issues/1668).


A list of all bugs can be found in our [issue tracker](/dev/issues).
