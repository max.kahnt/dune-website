+++
title = "How to Contribute"
[menu.main]
identifier = "contributing"
parent = "dev"
weight = 1
+++

## How to Contribute
DUNE is a community project and we are happy about all forms of external contributions. There are many easy things that you can do, like

* read the documentation and tell us where and how it should be improved,
* try to install DUNE on your platform and report bugs if it doesn't work,
* fix bugs and send us patches.

If you decide to contribute code please head to our [GitLab Instance](http://gitlab.dune-project.org/) and get an account there. Please read the contribution guide shown by GitLab in all the core modules.

### Google Summer of Code
In 2013 Dune participated in GSoC as a mentoring organization for the first time.

We are happy to announce that we are involved in this year's GSoC, too:

* Xinyun is working on a grid with spline geometries using B-spline and NURBS
  geometries.
  Checkout the
  [screenshots](http://gsoc2016xinyun.blogspot.de/2016/06/b-spline-and-nurbs-geometry-interface.html)
  with according surfaces.
  Usually we don't see such smooth output with our methods.
  Blog: [http://gsoc2016xinyun.blogspot.de](http://gsoc2016xinyun.blogspot.de)
  Code repository: [https://gitlab.dune-project.org/Xinyun.Li/dune-iga](https://gitlab.dune-project.org/Xinyun.Li/dune-iga)
* Michael is developing Python bindings for the DUNE grid interface.
  As an example, he implemented a
  [cell-centered finite volume in Python](http://misg.github.io/gsoc2016/dune/2016/06/05/two-weeks-later),
  inspired by the corresponding example in the DUNE grid how-to, using his bindings.
  Blog: [http://misg.github.io/](http://misg.github.io)
  Code repository: [https://gitlab.dune-project.org/michael.sghaier/dune-corepy](https://gitlab.dune-project.org/michael.sghaier/dune-corepy)

### Project Ideas
The following is a list of things that we find interesting but that we probably will never have the time to do ourselves.

#### Additional Features

* Write a module with language bindings for dune-grid for languages other than C++. Especially Python would be interesting.
* Implement iterators over edges and faces in UGGrid. The challenge here is that, unlike elements and vertices, edges and faces don't exist as separate objects within UG.

#### Documentation
Tutorials are a good way to get started with Dune. A good tutorial for dune-istl would be of great help.

#### Infrastructure

* Invent a mechanism that extracts all finite element implementations from dune-localfunctions (plus their doxygen documentation), and renders them into a LaTeX document. This document should then in turn become a reference section in the dune-localfunctions tutorial. It seems that the doxygen-LaTeX functionality is too limited for this. You may have to play around with the XML feature of doxygen. See the brief discussion in .

#### Additional Grid Backends

* Write a wrapper for the [Racoon II](http://www.tp1.ruhr-uni-bochum.de/~jd/racoon/) grid manager. This is a parallel AMR grid manager for 2d and 3d.
* Write a wrapper for the [AmRoc](http://amroc.sourceforge.net/) grid manager. This is another AMR manager. Some initial work has been done. Ask on the mailing list for details.
* Write a parallel AMR grid manager that works in more than three space dimensions.
* Write a structured simplex grid for rectangular domains. 2d and 3d would be great, the brave can implement Freudenthal's algorithm for a general n-d simplex grid.

#### Meta Grids
Meta grids are DUNE grids which are parametrized by another grid. An example is SubGrid, which allows you to select a subset of elements of a DUNE grid and treat this subset as a grid in its own right. Here are a few other ideas for meta grids which may be useful.

* Write a meta grid which, given an affine grid with a parametrized boundary, yields an isoparametric approximation of the boundary.
