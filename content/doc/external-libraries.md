+++
title= "External Libraries"
[menu.main]
parent = "docs"
weight = 2
+++

### Installing External Libraries
There are various external libraries that you can use together with DUNE. These include grid managers, file format libraries, fast linear algebra packages and more. On this page we list some of them and explain how they are obtained and installed.

##### Grid Managers
<table>
<tr>
<td><a href="/doc/install-alberta">ALBERTA</a></td>
<td>A grid manager for 2d and 3d simplicial grid with bisection refinement. Needed to use the AlbertaGrid class.</td>
</tr>
<tr>
<td><a href="/doc/install-ug">UG</a></td>
<td>A sequential and parallel grid manager for 2d and 3d cube and simplex grids with nonconforming refinement. Needed to use the ALUGrid class.</td>
</tr>
</table>

##### File I/O
<table>
<tr>
<td><a href="/doc/install-libamiramesh">libamiramesh</a></td>
<td>For reading and writing grids and data in the AmiraMesh file format.</td>
</tr>
<tr>
<td><a href="/doc/install-gmsh">Gmsh</a></td>
<td>An open-source finite element mesh generator with basic CAD capabilities. Meshes can be imported by Dune.</td>
</tr>
</table>

##### Others
<table>
<tr>
<td><a href="/doc/install-psurface">psurface</a> </td>
<td>A library for handling boundary parametrizations.</td>
</tr>
</table>

##### dune-istl
<table>
<tr>
<td><a href="/doc/install-superlu">SuperLU</a></td>
<td>General purpose library for the direct solution of large, sparse, nonsymmetric systems.</td>
</tr>
</table>
