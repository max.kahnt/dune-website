+++
title = "Installing PSurface"
+++
### Installing psurface for DUNE
Psurface is a C++ library that handles piecewise linear bijections between triangulated surfaces. These surfaces can be of arbitrary shape and need not even to be manifolds.

To install the psurface library:

1. The psurface library can be downloaded as [source code](http://numerik.mi.fu-berlin.de/dune/psurface/index.php).
2. `autoreconf -isf`

  Creates files needed for installation.
3. `./configure`

  Configures the makefiles and install options.
  You can specify an installation path with `--prefix=path_to_the_psurface_install`.
  If you intent to use psurface together with libamiramesh, make sure you pass the install path to configure `--with-amiramesh=path_to_amiramesh` accordingly.

4. make

  Compiles the library.
5. make install

  Installs the headers and the library to your system.

After you have successfully built the library add `--with-psurface=path_to_the_psurface_install` as an option to configure of DUNE.

For the dune-2.1-compatible branch of of dune-grid-glue you need the 1.2.0 release of psurface. If you want to work with the dune-2.0-compatible branch of dune-grid-glue you need the 1.1.6 release.
