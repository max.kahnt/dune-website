+++
title = "Beginners Resources"
[menu.main]
identifier = "beginnersresources"
parent = "docs"
weight = 0
+++

# How to get started with Dune

You want to install and use Dune on your machine and are a complete
novice? Then this is the right place to start!

We will give a short introduction to Dune and guide you through the
installation process.

Dune is a software framework  for the numerical solution of partial
differential equations (PDEs) written in C++. This means that it provides a
set of classes that help you to write your own PDE solver. It is not
an application with a fancy graphical user interface that you can just
run, type in your PDE and look at the result. Instead you write a C++
program that includes various pieces from the framework and glues them
together to solve a particular PDE with a particular method. It is,
however, quite flexible in letting you implement various different
solution procedures.

There are a number of different ways how to install and use Dune on
your computer (*click on the links to follow the instructions*):

1. Follow the detailed instructions in Oliver Sander's document on
[how to get started with Dune.](http://www.math.tu-dresden.de/~osander/research/sander-getting-started-with-dune.pdf)
This document describes installation from binary packages and
installation from the source repository. It also describes how to
solve a simple PDE with the Dune core modules.

2. [Installation of Dune 2.4 (including the grid howto) from binary packages on Ubuntu 16.04
   LTS.](/doc/beginners-resources-binary) This is the most convenient way if you have such a system (or
   can set it up in a virtual machine) and do not want to modify the
   Dune sources (and submit the changes to the community).

3. [Installation from source via a shell script](/doc/beginners-resources-script) which is the proper way if binary packages
   are not available for your machine and/or you want to modify the Dune
   sources.

Instructions for installing [dune-PDELab](/modules/dune-pdelab) from
script are available [here](/doc/beginners-resources-pdelab).

## Computer Requirements

It is important to note that installation of Dune requires a not too
old computer system with a relatively recent operating system, either
Linux or Apple MacOS. Windows is not officially
supported. Installation on Windows has been
managed by some but it is definitely not the subject of beginners resources!

If you are running Windows then probably the best approach is to
set up a virtual machine using
[VirtualBox](https://www.virtualbox.org) and install Linux in it,
e.g. [Ubuntu Linux](http://www.ubuntu.com).
