+++
title = "Installation"
[menu.main]
parent = "docs"
weight = 1
+++

### Installation Notes
These installation notes explain how to use CMake. If you stick with the legacy Autotools build-system, use the [old instructions](/doc/installation-old).

#### Dependencies
In order to build DUNE you need at least the following software:

* a standard compliant C++ compiler, tested are g++ and Clang; we try to stay compatibe with ICC
* CMake
* pkg-config

Detailed information on supported compiler and CMake versions
can be found in the [release notes](/releases) for releases
and in the list [recent changes](/dev/recent-changes) for the
development branch *master*.

The following software is recommend but optional:

* MPI (either OpenMPI, lam, or mpich suffice)

This will provide you with the core DUNE features.

Some DUNE modules might support further software. Using this software is optional. The dune-grid module for example supports different external grid managers like Alberta or UG; these have to be downloaded separately. For a list of supported contrib software packages and their installation see the notes on [Installation External Libraries](/doc/external-libraries).

#### Installing the core DUNE modules

Suppose you have downloaded all DUNE modules of interest to your computer and extracted then in one common directory. See the [download](/download) section for a list of available modules.

To compile the modules DUNE has to check several components of your system and whether prerequisites within the modules are met. For the ease of users we have designed a custom build system on top of CMake. Run

```
./dune-common/bin/dunecontrol all
```

to commence those tests and build all modules you have downloaded. Don't worry about messages telling you that libraries are missing: they are only needed for grid-self-checks we need for developing or for additional features.

You can customize the build to your specific needs by using an options file (see below)

```
./dune-common/bin/dunecontrol --opts=/path_to/file.opts
```

If you did not tell dunecontrol to install with an options file you need to run

```
./dune-common/bin/dunecontrol make install
```

to install DUNE (you may need root-permissions for the install part depending on the prefix set)

The [CMake FAQ](/doc/buildsystem/cmakefaq.pdf) provides additional helpful informations.

#### Building a Specific DUNE Module (and its dependent modules)
You can instruct dunecontrol to build only a certain dune module, using the `--only=<module_name>` switch. Runnning dunecontrol script
```
./dune-common/bin/dunecontrol --only=<module_name> all
```

where `<module_name>` is the name of that particular module given in the dune.module file, will build only the module `<module_name>`.

If you want to build a module and the modules it depends on, you must run:
```
./dune-common/bin/dunecontrol --module=<module_name> all
```

Read [Maintaining new DUNE Modules and Applications](/modules/dune-module) section for more details.

#### Passing Options to the Build Process
Using the dunecontrol script the following atomic commands can be executed:

* configure: runs the CMake configure tests for each module
* exec: executes a command in each module directory
* bexec: executes a command in each module's build directory
* make: runs make (or any other build tool that was configured) for each module
* update: pull the latest version from the Git repository

The composite command `all` simply runs `autogen, configure` and `make` for each module.
As it is often not convenient to specify the desired options after the duncontroll call, one can pass the options via file specified by the `--opts=<file>` option. Specify the options via the variable `CMAKE_FLAGS=<flags>`

An example of an options file is
```
# use a special compiler (g++ version 5.0),
# install to a custom# directory, default is /usr/local/bin,
# disable the external library SuperLU,
# and use Ninja-build instead of make as the build-tool
CMAKE_FLAGS="-DCMAKE_CXX_COMPILER=g++-5 -DCMAKE_INSTALL_PREFIX='/tmp/HuHu' -DCMAKE_DISABLE_FIND_PACKAGE_SuperLU=true -GNinja"
```
On some platforms special care might have to be taken to make things compile, e.g. this is the case for IBM's Blue Gene System P. If you want to run DUNE there please read the [instructions](/doc/installation-bluegenep) of Markus and save yourself a lot of time.

#### Creating Your Own DUNE Project Module
You can create your own dune project module by using the duneproject script available in `dune-common/bin` directory. Running the script will create a directory with supporting files (CMakeLists.txt etc.) and a sample .cc file. After creating the module you can build this as explained above under "Building a specific DUNE module".

The [DUNE Build System Howto](/doc/buildsystem/buildsystem.pdf) will also give you an excellent introduction to the build system and how to create new modules/projects your own.
