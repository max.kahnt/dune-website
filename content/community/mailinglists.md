+++
title = "Mailing Lists"
[menu.main]
parent = "community"
weight = 2
+++

### Mailing Lists
The DUNE-Developers can be contacted via the mailing list
[dune@dune-project.org](mailto:dune@dune-project.org). If you have a
question or a problem, don't hesitate to send us a mail. You can also
send us bug-reports, but usually the bug-tracking system
[via gitlab](https://gitlab.dune-project.org/groups/core/issues) is
the more appropriate place for that. When you send us a problem or bug
report, please observe the
[guidelines for bug reporting](/doc/guides/bug_reporting). Especially,
please use the list instead of mailing the developers individually,
unless you have a good reason to keep your mail private.

Currently the following mailing lists are used for the DUNE-project:

<table>
<tr>
<td><a href="mailto:dune@dune-project.org">dune@dune-project.org</a></td>
<td>Mailing list for user questions and discussions</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune">Subscribe/Unsubscribe</a></td>
<td><a href="http://lists.dune-project.org/pipermail/dune/">Archive</a></td>
</tr>

<tr>
<td><a href="mailto:dune-devel@dune-project.org">dune-devel@dune-project.org</a></td>
<td>Mailing list for discussions about the development of Dune itself</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-devel">Subscribe/Unsubscribe</a></td>
<td><a href="http://lists.dune-project.org/pipermail/dune-devel/">Archive</a></td>
</tr>

<tr>
<td><a href="mailto:dune-bugs@dune-project.org">dune-bugs@dune-project.org</a></td>
<td>A read only list where all bug-tracker mails go to</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-bugs">Subscribe/Unsubscribe</a></td>
<td>No Archive</td>
</tr>

<tr>
<td><a href="mailto:dune-announce@dune-project.org">dune-announce@dune-project.org</a></td>
<td>This mailing list notifies about important news regarding Dune.</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-announce">Subscribe/Unsubscribe</a></td>
<td><a href="http://lists.dune-project.org/pipermail/dune-announce/">Archive</a></td>
</tr>

<tr>
<td><a href="mailto:dune-commit@dune-project.org">dune-commit@dune-project.org</a></td>
<td>All git commit messages are posted here</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-commit">Subscribe/Unsubscribe</a></td>
<td>No Archive</td>
</tr>

<tr>
<td><a href="mailto:dune-fem@dune-project.org">dune-fem@dune-project.org</a></td>
<td>Mailing list for developers and users of the DUNE-FEM module</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-fem">Subscribe/Unsubscribe</a></td>
<td><a href="http://lists.dune-project.org/pipermail/dune-fem/">Archive</a></td>
</tr>

<tr>
<td><a href="mailto:dune-pdelab@dune-project.org">dune-pdelab@dune-project.org</a></td>
<td>Mailing list for developers and users of the DUNE-PDELAB module</td>
<td><a href="http://lists.dune-project.org/mailman/listinfo/dune-pdelab">Subscribe/Unsubscribe</a></td>
<td><a href="http://lists.dune-project.org/pipermail/dune-pdelab/">Archive</a></td>
</tr>

</table>


### To Subscribe
Either use the above links and use the mailman interface, or send a mail to `<list-name>-request@dune-project.org` with the subject subscribe. Please write the name of the list instead of the `<list-name>` words. Leave the body of the message empty. Do not include a signature or other stuff which might confuse the mail server which processes your request.

### To Unsubscribe
Either use the above links and use the mailman interface, or send a mail to `<list-name>-request@dune-project.org` with the subject unsubscribe. Please replace `<list-name>` with the name of the list you subscribed to. Leave the body of the message empty. Do not include a signature or other stuff which might confuse the mail server which processes your request.

### Notes
Please do not use HTML to send your request because it makes it difficult for our mail server to process your request. If the subscription address is identical to the address you are posting from you may omit the address in the subject line.
