Developer Meetings 2012
=======================

Date and Agenda
---------------

Thursday, June 21, until 17:00

Venue
-----

http://pdesoft2012.uni-muenster.de/location/index.html

Participants
------------

-   Felix Albrecht
-   Markus Blatt
-   Christian Engwer
-   Jö Fahlke
-   Christoph Grüninger
-   Olaf Ippisch
-   Robert Klöfkorn
-   Steffen Müthing
-   Mario Ohlberger
-   Oliver Sander
-   Carsten Gräser

Topics for the DUNE meeting 2012 in Münster
===========================================

Flyspray tasks
--------------

-   [FS\#959](http://www.dune-project.org/flyspray/index.php?do=details&task_id=959)
    Separate grid and geometry parts from
    (Basic-/Generic-)Geometry (Oli)
    -   **wird von Martin/Oli gemacht**

<!-- -->

-   [FS\#957](http://www.dune-project.org/flyspray/index.php?do=details&task_id=957)
    Remove grid dimension from BasicGeometry (Oli)
    -   **wird von Martin/Oli gemacht**

<!-- -->

-   [FS\#674](http://www.dune-project.org/flyspray/index.php?do=details&task_id=674)
    strict-aliasing warnings with gcc-4.4 (Carsten)
    -   **problematische Stellen werden rausgeschmissen, Carsten
        erstellt eine Liste**

<!-- -->

-   [FS\#587](http://www.dune-project.org/flyspray/index.php?do=details&task_id=587)
    GenericReferenceElements should use unsigned for indices, sizes etc.
    (Christi) + Frage der typen für dimension
    -   **für GenericReferenceElement etc unsigned size\_type**
    -   **für Dimensionen (Templates) int, da Auswirkungen nicht
        absehbar, vielleicht später auch unsigned**
        {CHG}

<!-- -->

-   [FS\#699](http://www.dune-project.org/flyspray/index.php?do=details&task_id=699)
    Wishlist: a general dynamic parameter interface for grids

<!-- -->

-   [FS\#1104](http://www.dune-project.org/flyspray/index.php?do=details&task_id=970)
    Remove EntityPointer in favour of improved Entity objects-
    -   **Ausprobieren**
    -   **Nach aktuellem Kentnissstand spricht nichts dagegen**
        {CHG}
    -   **Wäre gutes Beispiel für ein Feature-Branch**
        {CHG}

<!-- -->

-   [FS\#723](http://www.dune-project.org/flyspray/index.php?do=details&task_id=723)
    method order on basis
    -   **vertagt, da nicht wichtig genug**
        {CHG}

<!-- -->

-   [FS\#1011](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1011)
    operator\* in densevector used as dot product in dune-istl is not
    suitable for complex vectors (Oli)
    -   **Wir wollen so etwas wie, soll aber noch im FlySpray diskutiert
        werden: möglichst**
    -   **analog zu matlab oder python dot() Methode auf Vektoren
        einführen + freie Methode, M. Wohlmuth**
    -   **auerdem Verhalten von operatorStern dokumentieren**

<!-- -->

-   [FS\#987](http://www.dune-project.org/flyspray/index.php?do=details&task_id=987)
    build system: do not force all modules to place their headers in
    \$DUNE\_MODULE\_ROOT/dune/ (Christoph)
    -   **Meinungsbild: 2 für beliebige header, 11 für keine nderung, 1
        egal**

Other topics
------------

### Policy for integrating new grid implementations into DUNE:

Should/Can other grids be integrated in dune-grid or should every grid
get its own module.

-   OS: Why not have many grids in dune-grid, just as the Linux kernel
    contains many many drivers?
-   OS: In particular, I propose to move SPGrid into dune-grid, and get
    rid of YaspGrid and SGrid out in exchange. By all accounts SPGrid is
    vastly superior. I am sure we can figure out a way to distribute the
    maintenance burden
    -   **Prinzipielle Zustimmung aller** {CHG}

### Licence of DUNE: Should/Can we move to GPL version 3?

-   CE: Discussing with Debian, we should only consider a dual
    licensing model.
-   CE: we should not consider GPL3, but LGPL3, because updated the
    license to account for template libs **Dual License, alt + LGPL3+,
    wird angegangen, Christian Engwer**

### Report on the current state of `cmake`?

-   [FS\#1000](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1000)
-   [Status](/404.html) **(Update: cmake is now the default
    buildsystem and the old status page doesn't exist anymore)**
-   **Markus und Christoph bauen CMake für die verbleibenden Core-Module
    um, Entscheidung danach** {CHG}

### Should we stop enforcing Barton-Nackman for DUNE grids?

-   (Ich kann mich an das Ergebnis nicht erinnern, es wurde aber
    angesprochen, zumindest im Zusammenhang mit den Facades,
    siehe unten) {CHG}

### Future quality control, automated testing, and release procedure.

-   **wir wollen automatische tests, system ist noch unklar**
-   **Markus schaut sich dashboard an**
-   **der Mac soll wieder tun**
-   **das bisherige System soll gefixt werden, wenn möglich (Christian,
    Andreas, Markus)**

### Communication between developers

-   **Voting per Mailingliste oder Redmine oder coolem Votingsystem**
-   **auerdem Mailinglisten dune-bugs, dune-devel, dune-vote**

### Version Control System

(CE: I think these topics strongly interact)

-   Wir wollen feature branches einsetzten, also für ein Feature wird
    ein neuer Branch erstellt und dann kann verglichen werden und
    eventuell der Branch eingepflegt (merge) werden
    {CHG}
-   Switch to git VCS?
    -   bei svn bleiben: 0
    -   auf git umsteigen: Rest
    -   Enthaltung: 2 (*Martin?* Peter?)
    -   Vertagen: 0
    -   **Steffen fängt mal mit einer Anleitung an**
    -   Umstellung lieber am Stück

### OS: What happens to the GridFactory::insertionOrder methods?

Right now they are unusable, because no file reader hands out the grid
factory. OTOH having each reader hand out the factory may complicate the
codes much more than simply requiring each grid to have the level 0
index numbering in insertion order.

-   **Der Reader soll die Factory zurück geben**
-   **Wir wollen etwas allgemeineres haben als jetzt um Punkte zu
    verschieben/ hinzuzufügen, boundaries zu parametrisieren -&gt;
    Kleingruppe**

### Namespaces

-   OS: We need a namespace for each dune modul. How should the names
    for these namespace be?
-   Jö: How should the transition work?
    -   **wir wollen Namespaces der Form
        Dune::{Common,Grid,ISTL,LocalFunctions,Geometry}**
    -   **Tool von Martin / Felix soll bei der Umstellung helfen**
        {CHG}
    -   **Wird nur mit einem potentiellen Dune 3.0 kommen**
        {CHG}
    -   **Es wurde kein Zeitplan vereinbart**
        {CHG}

### Coding style

OS: We may want to discuss a few coding-style issues. Here is my
complete list, which is of course much to long

-   In some core modules a mixture of CamelCase and stl-style naming
    is used. A part of this comes from an attempt to maintain
    stl compatibility. Can we switch the code that is not intended to be
    stl compatible over to Dune (i.e., camel-case) naming? Or should we
    switch over to stl-naming altogether?
-   Naming of exported types, e.g. typedef GV GridView; vs. typedef GV
    GridViewType;
-   Naming of the files that contain unit tests. Should they be named
    testfoo.cc or footest.cc or something else?
-   The types of the two grid view types (leaf views and level views)
    are exported by each grid as LeafGridView and
    LevelGridView, respectively. However, the corresponding methods are
    called leafView and levelView.
-   The GenericReferenceElements have their 'Generic'-prefix only for
    historical reasons. It should be removed.
-   What is the name of directory inside the 'dune' directory of a
    module? Is it always the module name without the 'dune-' prefix?
-   [FS\#970](http://www.dune-project.org/flyspray/index.php?do=details&task_id=970)
    Unify typedefs -- XyzType vs. Xyz

**Wurde aus Zeitmangel nicht besprochen (?)**
{CHG}

### Facades für Dune-Gitter

-   **werden beibehalten**
-   **Tests werden modularisiert**
    -   **Traits sollten benutzt werden?**

Remarks
========

All entries that are marked with {CHG} were added or modified by
Christoph Grüninger from memory three weeks later (2012-07-13)
