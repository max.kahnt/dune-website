+++
title = "People"
[menu.main]
parent = "community"
weight = 1
+++

Here is an (incomplete) list of people who are actively contributing to Dune, or who have actively contributed in the past.

If you have questions about Dune, please send them to the [mailing list](/community/mailinglists), unless you have reason to keep your mail private.

* [Peter Bastian][1a] ([IWR, Universität Heidelberg][1b])
* [Markus Blatt][2a] ([Dr. Markus Blatt - HPC-Simulation-Software & Services, Heidelberg][2b])
* [Andreas Dedner][3a] ([Warwick Mathematics Institute, University of Warwick, UK][3b])
* [Christian Engwer][4a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][4b])
* [Jorrit Fahlke (Jö)][5a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][5b])
* [Christoph Gersbacher][6a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][6b])
* [Carsten Gräser][7a] ([Institut für Mathematik, Freie Universität Berlin][7b])
* [Christoph Grüninger][8a] ([Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart][8b])
* [Dominic Kempf][9a] ([IWR, Universität Heidelberg][9b])
* [Robert Klöfkorn][10a] ([International Research Institute of Stavanger, Norway][10b])
* [Steffen Müthing][11a] ([IWR, Universität Heidelberg][11b])
* [Martin Nolte][12a] ([Abteilung für Angewandte Mathematik, Universität Freiburg][12b])
* [Mario Ohlberger][13a] ([Institut für Numerische und Angewandte Mathematik, Universität Münster][13b])
* [Oliver Sander][14a] ([TU Dresden][14b])


[1a]: http://conan.iwr.uni-heidelberg.de/people/peter/
[1b]: http://conan.iwr.uni-heidelberg.de/
[2a]: http://www.dr-blatt.de/
[2b]: http://www.dr-blatt.de/
[3a]: http://www2.warwick.ac.uk/fac/sci/maths/people/staff/andreas_dedner/
[3b]: http://www2.warwick.ac.uk/fac/sci/maths/
[4a]: http://wwwmath.uni-muenster.de/u/christian.engwer/
[4b]: http://wwwmath1.uni-muenster.de/num/
[5a]: http://wwwmath.uni-muenster.de/num/Arbeitsgruppen/ag_engwer/organization/fahlke/
[5b]: http://wwwmath1.uni-muenster.de/num/
[6a]: http://aam.uni-freiburg.de/abtlg/wissmit/agkr/gersbacher/
[6b]: http://www.mathematik.uni-freiburg.de/IAM/
[7a]: http://page.mi.fu-berlin.de/graeser/
[7b]: http://www.math.fu-berlin.de/rd/we-02/numerik/RESEARCH/ag-kornhuber.html
[8a]: http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1450
[8b]: http://www.iws.uni-stuttgart.de/
[9a]: http://conan.iwr.uni-heidelberg.de/people/dominic/
[9b]: http://conan.iwr.uni-heidelberg.de/
[10a]: http://www.iris.no/
[10b]: http://www.iris.no/
[11a]: http://conan.iwr.uni-heidelberg.de/
[11b]: http://conan.iwr.uni-heidelberg.de/
[12a]: http://portal.uni-freiburg.de/aam/abtlg/wissmit/agkr/nolte/
[12b]: http://www.mathematik.uni-freiburg.de/IAM/
[13a]: http://wwwmath1.uni-muenster.de/u/ohlberger/
[13b]: http://wwwmath1.uni-muenster.de/num/
[14a]: https://www.math.tu-dresden.de/~osander/
[14b]: https://www.math.tu-dresden.de/~osander/
