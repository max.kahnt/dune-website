+++
title = "Grid-Manager Features"
[menu.main]
parent = "Features"
weight = 2
+++

Here is a collection of features of Dune's grid managers. These are the
features the grid manager provides via its Dune interface -- these may
be different from the features provided by the underlying external grid
library.

General Features
----------------

**fundamental?**: Whether the grid is a stand alone grid (yes) or wraps
another Dune grid (no).

  Name                                                             | fundamental?       | dimensions                 | structured?       | Notes
  ---------------------------------------------------------------- | ------------------ | -------------------------- | ----------------- | -----------------------------------------------------------------------------------------
  AlbertaGrid<dim,dimw>                                            | yes                | 1&lt;=dim&lt;=dimw&lt;=9   | unstructured      | Can't have two grids of different dimw in the same binary
  ALUGrid<dim,dimw,simplex,conforming>                             | yes                | 2&lt;=dim&lt;=dimw&lt;=3   | unstructured	   |
  ALUGrid<dim,dimw,cube,nonconforming>                             | yes                | 2&lt;=dim&lt;=dimw&lt;=3   | unstructured	   |
  ALUGrid<dim,dimw,simplex,nonconforming>                          | yes                | 2&lt;=dim&lt;=dimw&lt;=3   | unstructured	   |
  GeometryGrid<HostGrid>                                           | no                 | hostdim=dim&lt;=dimw       | unstructured	   |
  OneDGrid                                                         | yes                | dim=dimw=1                 | unstructured	   |
  UGGrid<dim>                                                      | yes                | 2&lt;=dim=dimw&lt;=3       | unstructured      | UG with MPI: no more than one grid object of the same dim may be live at any given time
  SGrid<dim,dimw>                                                  | yes                | dim&lt;=dimw               | structured		   |
  SPGrid<ct,dim,rs,comm>                                           | yes                | dim=dimw                   | structured		   |
  YaspGrid<dim>                                                    | yes                | dim=dimw                   | structured        | lower left corner fixed to 0.
  YaspGrid&lt;dim, EquidistantOffsetCoordinates<ctype, dim> &gt;   | yes                | dim=dimw                   | structured        | arbitrary lower left corner (introduced in 2.4)
  YaspGrid&lt;dim, TensorProductCoordinates<ctype, dim> &gt;       | yes                | dim=dimw                   | structured        | (introduced in 2.4)

Packaging and License
---------------------

**interface wrapper?**: Whether the is a wrapper around an external grid
manager or is implemented in Dune itself.\
**external module?**: Whether the grid is provided by `dune-grid` or an
additional dune-module is needed.

  Name                     | interface wrapper?       | external module?       | Availability
  ------------------------ | ------------------------ | ---------------------- | ------------------
  AlbertaGrid<dim,dimw>    | yes                      | no                     | free
  ALUGrid<dim,dimw,*,*>    | no                       | yes (dune-alugrid)     | free
  GeometryGrid<HostGrid>   | no                       | no                     | free
  OneDGrid                 | no                       | no                     | free
  UGGrid<dim>              | yes                      | no                     | free
  SGrid<dim,dimw>          | no                       | no                     | free
  SPGrid<ct,dim,rs,comm>   | no                       | yes (dune-spgrid)      | free
  YaspGrid<dim>            | no                       | no                     | free

Supported Entities and Iterators
--------------------------------

  Name                          | Element types                        | Entities for codim       | Iterators for codim
  ----------------------------- | ------------------------------------ | ------------------------ | -------------------------
  AlbertaGrid<dim,dimw>         | simplices                            | all                      | all
  ALUGrid<dim,dimw,simplex,*>   | simplices                            | all                      | all
  ALUGrid<dim,dimw,cube,*>      | cube                                 | all                      | all
  GeometryGrid<HostGrid>        | =host                                | all                      | all
  OneDGrid                      | lines                                | all                      | all
  UGGrid<dim>                   | simplices, prisms, pyramids, cubes   | all                      | 0, dim
  SGrid<dim,dimw>               | cubes                                | all                      | all
  SPGrid<ct,dim,rs,comm>        | cubes                                | all                      | all
  YaspGrid<dim>                 | cubes                                | all                      | all

Refinement
----------

  Name                                      | Refinement       | Strategy                      | conforming?       | boundary adaptation
  ----------------------------------------- | ---------------- | ----------------------------- | ----------------- | -------------------------
  AlbertaGrid<dim,dimw>                     | adaptive         | recursive bisection           | yes               | yes
  ALUGrid<dim,dimw,simplex,conforming>      | adaptive         | bisection                     | yes               | yes
  ALUGrid<dim,dimw,cube,nonconforming>      | adaptive         | red                           | no                | yes
  ALUGrid<dim,dimw,simplex,nonconforming>   | adaptive         | red                           | no                | yes
  GeometryGrid<HostGrid>                    | =host            | =host                         | =host             | =host
  OneDGrid                                  | adaptive         | ?                             | yes               | --
  UGGrid<dim>                               | adaptive         | red/green                     | selectable        | yes
  SGrid<dim,dimw>                           | global only      | 2\^dim children               | yes               | no
  SPGrid<ct,dim,rs,comm>                    | global only      | bisection, red, anisotropic   | yes               | no
  YaspGrid<dim>                             | global only      | 2\^dim children               | yes               | no

Parallel Features
-----------------

  Name                                    | parallel?       | overlap?       | ghosts?       | communicate() for codim       | dynamic load balancing
  --------------------------------------- | --------------- | -------------- | ------------- | ----------------------------- | ----------------------------
  AlbertaGrid<dim,dimw>                   | no              | --             | --            | --                            | --
  ALUGrid&lt;2,dimw,**,**&gt;             | no              | --             | --            | --                            | --
  ALUGrid&lt;3,3,\*,nonconforming&gt;     | yes             | no             | yes           | all                           | yes
  ALUGrid&lt;3,3,simplex,conforming&gt;   | yes             | no             | no            | all                           | yes
  GeometryGrid<HostGrid>                  | =host           | =host          | =host         | =host                         | =host
  OneDGrid                                | no              | --             | --            | --                            | --
  SGrid<dim,dimw>                         | no              | --             | --            | --							 |
  SPGrid<ct,dim,rs,comm>                  | yes             | yes            | no            | all                           | no
  UGGrid<dim>                             | yes             | no             | yes           | 0, dim						 |
  YaspGrid<dim>                           | yes             | yes            | no            | all                           | no
