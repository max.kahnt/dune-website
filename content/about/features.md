+++
title = "Features"
[menu.main]
parent = "about"
weight = 2
+++

### DUNE Features
The following list gives a short overview over the main features of DUNE and refers you to more detailed documentation.

+ **Grid Implementation**
So far seven grid implementations are available through the DUNE grid interface. Each is geared towards a different purpose and thus has a different [set of features](../gridmanager-features). Further grid managers can be obtained by downloading [additional modules](/modules/grid-modules).

 * [AlbertaGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___alberta_grid.html): The grid manager of the Alberta toolbox
 * [ALUConformGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___a_l_u_conform_grid.html): [ALUCubeGrid](): [ALUSimplexGrid](), a family of grids in two and three space dimension based on the ALUGrid library.
 * [GeometryGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___geo_grid.html): A metagrid exchanging the host grid's geometry
 * [OneDGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___one_d_grid.html): A sequential locally adaptive grid in one space dimension
 * [SGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___s_grid.html): A structured grid in n space dimensions
 * [UGGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___u_g_grid.html): The grid manager of the UG toolbox
 * [YaspGrid](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___yasp_grid.html): A structured parallel grid in n space dimensions

+ **Linear Algebra**
DUNE contains ISTL (the Iterative Solver Template Library) for powerful (parallel) linear algebra. The main features are:
 * Abstractions for block matrices (e.g. [compressed row storage](http://www.dune-project.org/doc/doxygen/dune-istl-html/classDune_1_1BCRSMatrix.html) and [block diagonal](http://www.dune-project.org/doc/doxygen/dune-istl-html/classDune_1_1BDMatrix.html)) and [block vectors](http://www.dune-project.org/doc/doxygen/dune-istl-html/classDune_1_1BlockVector.html)
 * Block structure arbitrarily nestable
 * High performance through generic programming
 * Expression templates for BLAS1 routines
 * Several standard solvers (Krylov methods and stationary iterative methods)
 * Highly scalable parallel algebraic multigrid method available as preconditioner.

+ **Quadrature Formulas**
 * Quadrature rules for all common element types
 * Rules for hypercubes up to order 19, for simplices up to order 12
 * Easy access

+ **Shape Functions**
 * Lagrangian shape functions of arbitrary order
 * Monomial shape functions of arbitrary order for Discontinous Galerkin methods
 * Orthonormal shape functions of arbitrary order
 * Raviart-Thomas shape functions of lowest order for cube and of arbitrary order for simplex elements

+ **Input/Output**
 * Reading grid files in the [gmsh](http://www.geuz.org/gmsh/) format
 * Reading grid files in the grid independent Dune grid format [DGF](http://www.dune-project.org/doc/doxygen/dune-grid-html/group___dune_grid_format_parser.html)
 * Reading simplex grids through DGF constructed using the tools [Tetgen](http://tetgen.berlios.de/) and [Triangle](http://www.cs.cmu.edu/~quake/triangle.html)
 * Reading and writing in the AmiraMesh format
 * Subsampling of high-order functions
 * Write grids and data in the format of the visualization toolkit (vtk)
 * Visualization using [GRAPE](http://www.mathematik.uni-freiburg.de/IAM/Research/grape/GENERAL/)
 * Output in Data Explorer format

+ **Discretization Module**
There are some [discretization modules](/modules/disc-modules) available which provide
 * Arbitrary order lagrange spaces for different grid structures
 * Discontinuous Galerkin spaces of arbitrary order both with orthonormal and Lagrange basis function
 * Further discrete function spaces include Raviart-Thomas, Nedelec, and others
 * Linear and non-linear solvers
 * Time discretization method, e.g., explicit, implicit, and IMEX Runge-Kutta methods or multistep methods
 * Support for parallelization and adaptivity (both h and p refinement)
