+++
date = "2016-01-03T15:18:12+01:00"
title = "New paper on dune-functions module"
+++

We have just finished a new [article](http://arxiv.org/abs/1512.06136) that describes and benchmarks parts of the new
`dune-functions` module. The `dune-functions` module defines and implements interfaces for functions and function space
bases, providing a middle layer between the Dune core modules and discretization modules like [dune-pdelab](/modules/dune-pdelab) and `dune-fufem`.

The code and more information is available from the [module homepage](/modules/dune-functions).
