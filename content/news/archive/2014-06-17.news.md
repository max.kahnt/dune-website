+++
date = "2014-06-17"
title = "DUNE 2.3.1 Released"
+++

We are pleased to announce the maintainance release 2.3.1 of the Dune core modules.
 Dune 2.3.1 brings bug fixes and improvements for ParMETIS, SCOTCH, and CMake 3.0. And it gains tab completion for dunecontrol.

For further more details have a look at the [release notes](/releases/2.3.1).
