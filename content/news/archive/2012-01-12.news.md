+++
date = "2012-01-12"
title = "dune-fem 1.2.1 maintenance release"
+++

A maintenance 1.2.1 version of dune-fem has been released. It fixes various known issues of the dune-fem-1.2.0 release version.

The Dune-Fem module is based on the Dune-Grid interface library, extending the grid interface by a number of higher order discretization algorithms for solving non-linear systems of partial differential equations. For further information, please visit the [Dune-Fem section](/modules/dune-fem/) on this page or go to the [project homepage](http://dune.mathematik.uni-freiburg.de/) directly.
