+++
date = "2013-04-08"
title = "Dune is part of GSOC2013"
+++

![](/img/gsoc/gsoc13-logo-google-color.jpg)
 Google Summer of Code 2013
 [![Creative Commons License](http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png)](http://creativecommons.org/licenses/by-nc-nd/3.0/)

We are happy to let you know, that Dune has been accepted as a mentoring organization in [Google Summer of Code 2013](http://www.google-melange.com/gsoc/org/google/gsoc2013/dune). Google is offering students stipends to write code for Dune this summer. Starting from monday, april 22th, 12 UTC, you can apply as a student at [google-melange](http://www.google-melange.com/gsoc/homepage/google/gsoc2013).

We have put together a short list of [possible project ideas](/dev/gsoc/2013/), we had in mind. This list is not exclusive, so if you have a good idea, feel free to discuss with us.

Further information can be found on the official [GSOC2013 page of Dune](http://www.google-melange.com/gsoc/org/google/gsoc2013/dune).
