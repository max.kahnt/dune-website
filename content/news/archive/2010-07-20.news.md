+++
date = "2010-07-20"
title = "1st Dune User Meeting"
+++

With great help of Bernd Flemisch we can announce that the first Dune User Meeting October 6th-8th 2010 is taking place in (or close to) Stuttgart. Please see the [mail archive](http://lists.dune-project.org/pipermail/dune/2010-July/007164.html) for more detailed information.
