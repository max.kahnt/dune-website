+++
date = "2013-05-31"
title = "Google summer of Code students for Dune announced"
+++


![](/img/gsoc/gsoc13-logo-google-color.jpg)
 Google Summer of Code 2013
 [![Creative Commons License](http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png)](http://creativecommons.org/licenses/by-nc-nd/3.0/)

We are happy to let you know, that this years Google summer of code projects have been announced on May 28, 2013. Dune, as an accepted mentoring organization, is glad that two students will work on DUNE during summer.

Miha Čančula will work on a performance testing framework. This will allow us to regularly review the performance of sample simulations/programs and help us to prevent/identify changes that hurt the performance of our framework. This project is mentored by Christian Engwer and Christoph Grüninger.

Xiaoxue Gong will work on visualizing psurface objects used in DUNE in Paraview. The Python bindings of Paraview will be used to accomplish this goal. This project will be mentored by Oliver Sander and Markus Blatt.
