+++
date = "2007-12-20"
title = "Release of DUNE 1.0"
+++

We are pleased to announce the release of Version 1.0 of the "Distributed and Unified Numerics Environment" (DUNE).

DUNE is a modular toolbox for solving partial differential equations (PDEs) with grid-based methods. It supports the easy implementation of methods like Finite Elements (FE), Finite Volumes (FV), and also Finite Differences (FD).

DUNE is free software licensed under the GPL (version 2) with a so-called "runtime exception". This licence is similar to the one under which the libstdc++ libraries are distributed. Thus it is possible to use DUNE even in proprietary software.

For further information, have a look at our website <http://www.dune-project.org>

The framework consists of a number of modules which are downloadable as separate packages. The current core modules are:

 - **dune-common**

   > contains the basic classes used by all DUNE-modules. It provides some infrastructural classes for debugging and exception handling as well as a library to handle dense matrices and vectors.

 - **dune-grid**

   > defines nonconforming, hierarchically nested, multi-element-type, parallel grids in arbitrary space dimensions. Graphical output with several packages is available, e.g. file output to IBM data explorer and VTK (parallel XML format for unstructured grids). The graphics package Grape has been integrated in interactive mode.

 - **dune-istl (Iterative Solver Template Library)**

   > provides generic sparse matrix/vector classes and a variety of solvers based on these classes. A special feature is the use of templates to exploit the recursive block structure of finite element matrices at compile time. Available solvers include Krylov methods, (block-) incomplete decompositions and aggregation-based algebraic multigrid.
