+++
date = "2015-09-25T15:24:46+01:00"
title = "Core Module repositories moved to a GitLab instance"
+++

The repositories of the Dune core modules have moved to a GitLab instance hosted at IWR Heidelberg.
You find it here. The old repositories will be kept in read-only state for a while, but you should switch to
the new repositories soon. With this change we are also exploring new development models. Merge requests on
GitLab are our favorite way of receiving contributions from now on. Just get a GitLab account, fork the core
modules and open a merge request! Note, that also the issue tracker has moved from Flyspray to GitLab. All old
issues are accessible in the project flyspray/FS on GitLab.
