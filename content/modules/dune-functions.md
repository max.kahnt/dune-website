+++
git = "https://gitlab.dune-project.org/staging/dune-functions.git"
group = "extension"
module = "dune-functions"
requires = ["dune-localfunctions", "dune-grid", "dune-typetree"]
short = ""
suggests = ["dune-istl"]
title = "dune-functions"
+++

### dune-functions
The _dune-functions_ module provides an abstraction layer for global finite element functions. Its two main concepts are functions implemented as callable objects, and bases of finite element spaces.

#### Functions
_dune-functions_ provides an interface to "functions" in the mathematical sense, in particular to finite element functions defined on a grid, but going far beyond that.

The interface revolves around the concept of a "callable". It encompasses any type of C++ object that can be evaluated with `operator()`, like free functions, function objects, and even C++11 lambdas. Dynamic polymorphism is realized using type erasure and the `std::function` class, which does not sacrifice efficiency in purely static code.

_dune-functions_ extends the "callable" concept into several directions. First, it allows for differentiable functions. Such functions can hand out their derivative as new function objects. Second, for functions defined piecewisely on a finite element grid, the concept of local function is introduced. Local functions can be bound to grid elements. All further evaluations of a function bound to an element are in local coordinates of that element. This approach allows to avoid overhead when there are many evaluations on a single element.

#### Function Space Bases
The second part of dune-functions provides a well-defined interface to bases of finite element function spaces. For this interface, a finite element basis is a set of functions with a prescribed ordering, and a way to index them. The core functionality has three parts:

1. For a given grid element, obtain the restrictions of all basis functions to this element, except for those functions where the restriction is zero. In other words: get the shape functions for the element.
2. Get a local numbering for these shape functions. This is needed to index the element stiffness matrix.
3. Get a global numbering for the shape functions. This is needed to index the global stiffness matrix.

While local numbers are always integers, global numbers can be multi-indices, if appropriate.

<img src="/img/xyz.png" atl="missing image" width="400" height="300">

A central feature is that finite element bases for vector-valued and mixed spaced can be constructed by tensor multiplication of simpler bases. The resulting expressions can be interpreted as tree structures. For example, the tree for the three-dimensional Taylor-Hood basis is shown above. This tree structure is directly exposed in the dune-functions interface. An easy mechanism allows to construct new spaces.

##### Implementations of Function Space Bases
Currently, the following finite element bases are available:

* PQkNodalBasis: A k-th order Lagrangian bases, with k a compile-time parameter.
* LagrangeDGBasis: A k-th order DG basis, using Lagrangian shape functions.
* TaylorHoodBasis: The P2/P1 Taylor-Hood basis, for simplex-, cube-, and mixed grids.
* BSplineBasis: A basis of B-Spline functions of given arbitrary order on a structured grid.

### Download
You can download the current development version using anonymous git.
`git clone https://gitlab.dune-project.org/staging/dune-functions.git`

Since _dune-functions_ is still fairly new, it has not seen a formal release yet.  However, if you
would like to use _dune-functions_ together with the Dune 2.4 release, you can use the
_releases/2.4-compatible_ branch.

_dune-functions_ depends on the _dune-typetree_ module, available from the
[dune-pdelab](/modules/dune-pdelab) page. Please install that before trying to install <kbd>dune-functions</kbd>.

### Mailing lists
_dune-functions_ development and discussions happen mainly on the [dune-functions mailing list](http://lists.dune-project.org/mailman/listinfo/dune-functions).

### Maintainers
_dune-functions_ has been mainly written by

1. Christian Engwer
2. Carsten Gräser
3. Steffen Müthing
4. Oliver Sander

See the COPYING file contained in the source code for a complete list.

We welcome interest and contributions by additional developers.
