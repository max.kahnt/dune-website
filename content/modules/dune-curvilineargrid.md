+++
module = "dune-curvilineargrid"
group = "grid"
requires = ["dune-grid"]
maintainers = "LSPR AG, (curvilineargrid at lspr dot ch)"
git = "http://www.curvilinear-grid.org"
short = "CurvilinearGrid implements a 3d simplex grid based on Lagrangian interpolation. Features include flexible polynomial order, parallel mesh reading and partitioning, and curvilinear visualization via VTK."
+++
