+++
git = "https://gitlab.dune-project.org/pdelab/dune-typetree.git"
group = "extension"
maintainers = "Steffen Müthing"
module = "dune-typetree"
requires = ["dune-common"]
title = "dune-typetree"
+++
