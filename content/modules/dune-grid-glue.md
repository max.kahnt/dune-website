+++
git = "https://gitlab.dune-project.org/extensions/dune-grid-glue.git"
group = "extension"
module = "dune-grid-glue"
requires = ["dune-common", "dune-geometry", "dune-grid"]
short = "Provides infrastructure for the coupling of two unrelated Dune grids."
title = "dune-grid-glue"
+++

### dune-grid-glue
The <mark>dune-grid-glue</mark> module provides infrastructure for the coupling of two unrelated Dune grids. The coupling may be overlapping or nonoverlapping, conforming or nonconforming. The two grids are not requested to be of the same type, and they may even be of different dimensions. Here are a few possible scenarios:

<img src="/img/coupling_scenarios.png" class="center-block" style="width:100%; max-width:600px">

Couplings are described as sets of remote intersections. Conceptually, these remote intersections are very close to what the regular intersections in the Dune grid interface are, with the difference that the inside and outside entities are taken from different grids. You can iterate over the global set of remote intersections or over the ones pertaining to a given element.

<img src="/img/nonoverlapping_coupling.png" style="width:100%">


For our domain decomposition infrastructure we have tried to follow the Dune philosophy:

* We propose abstract interfaces to general grid coupling mechanisms, allowing to implement most existing domain decomposition algorithms.
* We allow and encourage the use of existing coupling implementations as legacy backends.
* We strive to make the code efficient, using generic programming where appropriate.

The actual computation of the remote intersections is handled by exchangeable backends. Currently, three backends are available:

* OverlappingMerge: For overlapping couplings in 1d, 2d, and 3d
* ContactMerge: For nonoverlapping couplings, including contact problems, where there is a positive distance between the two contact boundaries.
* ConformingMerge: A fast implementation for conforming nonoverlapping couplings.

All three backends are based on the optimal-time advancing front algorithm by Martin Gander and Caroline Japhet.

* M. Gander, C. Japhet, An Algorithm for Non-Matching Grid Projections with Linear Complexity, In 'Domain Decomposition Methods in Science and Engineering XVIII', Springer, 2009, pp. 185-192

### Download
_dune-grid-glue_ is available in [Debian](https://packages.debian.org/search?keywords=dune-grid-glue&searchon=names&suite=all&section=all), and possibly in other Linux distributions. You can download the current development version using anonymous git.
`git clone https://gitlab.dune-project.org/extensions/dune-grid-glue.git`

### Publications and Documentation
The concepts of _dune-grid-glue_ are presented in the following publication:

* P. Bastian, G. Buse, O. Sander, Infrastructure for the Coupling of Dune Grids, In 'Proceedings of ENUMATH 2009', Springer, 2010, pp. 107-114
* C. Engwer, S. Müthing, Concepts for flexible parallel multi-domain simulations, In 'Domain Decomposition Methods in Science and Engineering XXII', Springer
There is also a class-documentation created by [doxygen](http://www.stack.nl/~dimitri/doxygen/).

### Maintainers
_dune-grid-glue_ has been written by [Christian Engwer](http://wwwmath.uni-muenster.de/u/christian.engwer/) and [Oliver Sander](http://www.math.tu-dresden.de/~osander/), based on work by [Gerrit Buse](http://www5.in.tum.de/wiki/index.php/Dipl.-Inf._Gerrit_Buse). Lots of help has come from Ansgar Burchardt, Katja Hanowski, and Jonathan Youett.
