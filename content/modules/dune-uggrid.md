+++
module = "dune-uggrid"
git = "https://gitlab.dune-project.org/staging/dune-uggrid.git"
group = "grid"
maintainers = "Oliver Sander <oliver.sander@tu-dresden.de>, Ansgar Burchardt <ansgar.burchardt@tu-dresden.de>"
requires = ["dune-common"]
short = "This is a fork of the old [UG](http://www.iwr.uni-heidelberg.de/iwrwikiequipment/software/ug) finite element software, wrapped as a Dune module, and stripped of everything but the grid data structure. You need this module if you want to use the UGGrid grid implementation from dune-grid."
+++
