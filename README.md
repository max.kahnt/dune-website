# Requirements

You need these tools installed:
* git-lfs from https://git-lfs.github.com/ (Version 1.1.0 for now due to GitLab compatibility problems - [Download from here](https://github.com/github/git-lfs/releases/tag/v1.1.0))
* hugo from https://github.com/spf13/hugo/releases

# Setup

## git-lfs

After installing the dependencies, you need to make sure git-lfs is working correctly. For this purpose, run the following command in a terminal:
```
git lfs install
```
This command will set up the required hooks and filters in your configuration to integrate git-lfs with git.

## Cloning the repository

Use the following commands to clone the repository:
```
git clone --config lfs.fetchexclude=static https://gitlab.dune-project.org/infrastructure/dune-website.git
cd dune-website
git config --unset lfs.fetchexclude
```
It is **extremely important** to clone using `https`, otherwise LFS won't work.

## Credential helper

git-lfs requires you to set up a [credential helper](https://git-scm.com/docs/gitcredentials) that will supply it with the username and password
for gitlab.dune-project.org. There are multiple options, depending on your operating system:

* On Linux, you can either use the **credential cache** or the **credential store**:

    * The **credential cache** keeps your credentials in memory for a short while. If you want to use this, you will have to feed
      the credentials to the cache rather often.

    * If you don't want git to permanently remember your password, you can use the **credential store**. Note, however, that it stores
      the password in unencrypted form on your hard drive, so you probably want to use this only for selected remotes.

    Irrespective of which helper you want to use, you first have to set it up by running the following command in the dune-website repository:
    ```
    git config [--global] credential.helper [cache/store]
    ```
    To enable the helper only for remotes hosted on Dune's gitlab instance, use
    ```
    git config --global credential.https://gitlab.dune-project.org.helper [cache/store]
    ```
    If you add the `--global` option, git will use the same credential helper for all repositories on your machine.

    Afterwards, you have to run the script [bin/update-gitlab-credentials](bin/update-gitlab-credentials) to add your GitLab credentials
    to the helper.

    **Important**: If you are using the cache, you have to run [bin/update-gitlab-credentials](bin/update-gitlab-credentials) **every time** before
    you talk to the server (`git pull`, `git push` etc.).

* On macOS, git ships with a credential helper that integrates with the system keychain for encrypted password storage. You can enable
  that cache globally by running
  ```
  git config --global credential.helper osxkeychain
  ```
  Afterwards, you can store your credentials by running the script [bin/update-gitlab-credentials](bin/update-gitlab-credentials) once.

## Finishing up

After you have set up the credential helper and storing the credentials in the helper by running
[bin/store-gitlab-credentials](bin/store-gitlab-credentials), run the following command to finish up the repository setup:
```
git lfs pull
```
This will download all of the large binary files.

# Building
Having installed these requirements, type in the toplevel directory of dune-website:
```
hugo server --watch
```
This will spin up a webserver, allowing you to browse the page. The page will
update itself as soon as you apply any changes to the sources.

Building the homepage to actually deploying it is done through the project
infrastructure/dune-website-builder

# Directory structure
The following subdirectories of a hugo project are relevant when adding content:
* the contents folder contains content in form of markdown files.
  Each markdown file has a so-called front matter (separated by +++), that
  contains metadata of the page. For most normal contents, it is sufficient
  to specify the Title there. The structure within the content subdir
  is translated into the directory structure of the built homepage.
  (./content/about/dune.md appears at www.dune-project.org/about/dune)
* the static folder contains all sorts of static data. Git LFS is used
  to add binary data. You can simply commit big pdfs or tarballs, no problem.

# Archetypes

The following contents are managed through so called archetypes (see archetypes subdir):
* News items (called "news")
* Releases (called "releases")
* Dune Modules (called "modules")

Whenever you want to add content of a given archetype you can either
* `hugo new <archetype>/<name>.md` and then edit the newly generated file
* `cp archetypes/<archetype>.md content/<archetype>/<name>.md` and then edit

Both these approaches have their pros and cons. The former will automatically
add a `Date` field to the generated file, which is needed for the sorting of
news items. Then again, the latter preserves comments in the archetype frontmatter
(which help a lot in understanding the doxygen build process).

## News items

News items work exactly as before. The timestamp is automatically added to the front matter.

## Releases

The process of publishing a Dune releases is (or will be, as we will test all that with 2.4.1)
kind of automated through the releases archetype. The download section is populated automatically,
the markdown content is just the release notes. The menu automatically chooses the latest 3 releases
to link directly (excluding outdated point releases).

To add a new release, have a look at `bin/add_release.py`.

## Modules

I added an archetype "modules" to document dune modules. This has the following effects:
* Each dune module has its own content page, just as some chosen few had before (the discretization
  modules, dune-grid-glue, dune-mc, dune-functions etc.)
* A module summary is automatically shown in the corresponding module group (check the menu!)
* Crosslinking to all requirements etc.
I would like this to become some sort of a database of available dune modules.

To add new module to the "database" proceed as follows:
* `cp archetypes/modules.md content/modules/dune-foo.md` in the top-level directory
* Open content/modules/dune-foo.md and carefully read through the frontmatter and fill in the missing information.
* Write a text of arbitrary length and amount of detail below the frontmatter in markdown!

# Doxygen

Building the doxygen documentation is done from the project dune-website-builder.
The documentation building is triggered through tags in the frontmatter of either releases
or dune modules. Please read through the frontmatter of the modules archetype for details!

Doxygen documentation is currently built nightly through a cronjob!

# Online Editing
Many developers asked for an "online editing" system. This is
available through the small editing button in the top right of the
page. This is only possible for single content pages (e.g
https://beta.dune-project.org/about/dune/).  It uses gitlabs online
editing system.

# More information

The docs at www.gohugo.io are a good read.

If you have specific questions please contact
Dominic Kempf <dominic.kempf@iwr.uni-heidelberg.de>
