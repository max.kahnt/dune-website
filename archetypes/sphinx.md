+++
# The module from which to build the sphinx documentation
module = "dune-common"

# The location of the html sphinx documentation in the build tree
path = "doc/buildsystem/sphinx"

# The git branch to generate the documentation from
branch = "master"
+++
