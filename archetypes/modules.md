+++
# The name of the module.
module = "dune-foo"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = "user"

# List of modules that this module requires
requires = ["dune-common"]

# List of modules that this module suggests
suggests = []

# A string with maintainers to be shown in short description, if present.
#maintainers = ""

# Main Git repository, uncomment if present
#git = ""

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
#short = ""

# Doxygen documentation: Please specify the following keys to automatically build
# a doxygen documentation for this module. Note, that specifying the git key is
# necessary in this case. All of the keys should be lists of the same length.
# Each entry of the list specifies the parameter for a separate piece of documentation.
# You can use this feature to generate documentation for several branches.
#
# Specify the url, where to build the doxygen documentation
#doxygen_url = ["doxygen/mymodule"]
# Specify the branch from which to build, omit to build from master
#doxygen_branch = ["master"]
# Specify to build a a joint documentation from the following list of modules,
# omit, to build a doxygen documentation only for this module. This list will
# be used for all documentations, no list of lists necessary...
#doxygen_modules = []
# Please specify the name of the doxygen documentation, that will be shown on the main page.
#doxygen_name = ["Dune UNSTABLE"]

# Please add as many information as you want in markdown format directly below this frontmatter.
+++
